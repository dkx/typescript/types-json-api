# DKX/TypesJsonApi

Types definitions for [jsonapi.org](https://jsonapi.org/).

## Installation

```bash
$ npm install --save-dev @dkx/types-json-api
```
