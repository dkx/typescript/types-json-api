import {JsonApiMeta} from './meta';


export declare type JsonApiLink = string|JsonApiLinkObject;


export declare interface JsonApiLinkObject
{
	href?: string,
	meta?: JsonApiMeta,
}
