import {JsonValue} from '@dkx/types-json';

import {JsonApiMeta} from './meta';
import {JsonApiLink} from './links';


export declare type JsonApiRelationshipData = null|JsonApiResourceIdentifier|Array<JsonApiResourceIdentifier>;


export declare interface JsonApiResource
{
	id: string,
	type: string,
	attributes?: JsonApiAttributesList,
	relationships?: JsonApiRelationshipsList,
	links?: JsonApiResourceLinksList,
	meta?: JsonApiMeta,
}


export declare interface JsonApiAttributesList
{
	[key: string]: JsonValue,
}


export declare interface JsonApiRelationshipsList
{
	[key: string]: JsonApiRelationship,
}


export declare interface JsonApiRelationship
{
	links?: JsonApiRelationshipLinksList,
	data?: JsonApiRelationshipData,
	meta?: JsonApiMeta,
}


export declare interface JsonApiRelationshipLinksList
{
	self?: JsonApiLink,
	related?: JsonApiLink,
}


export declare interface JsonApiResourceIdentifier
{
	id: string,
	type: string,
	meta?: JsonApiMeta,
}


export declare interface JsonApiResourceLinksList
{
	self?: JsonApiLink,
}
