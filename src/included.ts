import {JsonApiResource} from './resource';


export declare type JsonApiIncludedList = Array<JsonApiResource>;
