import {JsonValue} from '@dkx/types-json';


export declare interface JsonApiMeta
{
	[key: string]: JsonValue,
}
