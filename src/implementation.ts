import {JsonApiMeta} from './meta';


export declare interface JsonApiImplementation
{
	version?: string,
	meta?: JsonApiMeta,
}
