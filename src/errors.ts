import {JsonApiMeta} from './meta';
import {JsonApiLink} from './links';


export declare type JsonApiErrorsList = Array<JsonApiError>;


export declare interface JsonApiError
{
	id?: string,
	links?: JsonApiErrorLinksList,
	status?: string,
	code?: string,
	title?: string,
	detail?: string,
	source?: JsonApiErrorSource,
	meta?: JsonApiMeta,
}


export declare interface JsonApiErrorLinksList
{
	about: JsonApiLink,
}


export declare interface JsonApiErrorSource
{
	pointer?: string,
	parameter?: string,
}
