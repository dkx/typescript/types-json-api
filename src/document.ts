import {JsonApiErrorsList} from './errors';
import {JsonApiImplementation} from './implementation';
import {JsonApiLink} from './links';
import {JsonApiMeta} from './meta';
import {JsonApiIncludedList} from './included';
import {JsonApiResource, JsonApiResourceIdentifier} from './resource';


export declare type JsonApiDocumentData =
	null|
	JsonApiResourceIdentifier|JsonApiResource|
	Array<JsonApiResourceIdentifier>|Array<JsonApiResource>;


export declare interface JsonApiBaseDocument
{
	jsonapi?: JsonApiImplementation,
	links?: JsonApiDocumentLinksList,
	included?: JsonApiIncludedList,
	meta?: JsonApiMeta,
}


export declare interface JsonApiDocument extends JsonApiBaseDocument
{
	data?: JsonApiDocumentData,
}


export declare interface JsonApiErrorDocument extends JsonApiBaseDocument
{
	errors?: JsonApiErrorsList,
}


export declare interface JsonApiDocumentLinksList
{
	self?: JsonApiLink,
	related?: JsonApiLink,
}
